﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Abarrotesmipueblito;
using AccesoDatos.Abarrotesmipueblito;

namespace LogicaNegocios.Abarrotesmipueblito
{
    public class ProveedorManejador
    {
         private ProveedorAccesoDatos _proveedorAccesoDatos = new ProveedorAccesoDatos();
        public void Guardar(Proveedor proveedor)
        {
            _proveedorAccesoDatos.Guardar(proveedor);
        }
        public void eliminar(int idProveedor)
        {
            _proveedorAccesoDatos.eliminar(idProveedor);
        }
        public List<Proveedor> GetProveedor(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
           
            var listProveedor = _proveedorAccesoDatos.GetProveedor(filtro);
            
            //Llenar lista
            return listProveedor;
        }

        /*
        private ProveedorAccesoDatos _proveedorAccesoDatos = new ProveedorAccesoDatos();
        public void Guardar2(Proveedor proveedor)
        {
            _proveedorAccesoDatos.Guardar(proveedor);
        }
        public void eliminar2(int nControl)
        {
            _alumnoAccesoDatos.eliminar(nControl);
        }
        public List<Alumno> GetUsuarios2(string filtro)
        {
            var listAlumno = _alumnoAccesoDatos.GetUsuarios(filtro);
            return listAlumno;
        }
        */
    }
}
