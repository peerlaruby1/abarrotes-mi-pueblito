﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.Abarrotesmipueblito;
using LogicaNegocios.Abarrotesmipueblito;

namespace Abarrotesmipueblitoo
{
    public partial class Frm_Proveedor : Form
    {
        DataSet ds = new DataSet();
        private ProveedorManejador _proveedorManejador;
        public Frm_Proveedor()
        {
            InitializeComponent();
            _proveedorManejador = new ProveedorManejador();
            //dtpFN.Format = DateTimePickerFormat.Custom;
            //dtpFN.CustomFormat = "yyyy-MM-dd";
        }

        private void lbl_buscar_Click(object sender, EventArgs e)
        {

        }

        private void txt_idproveedor_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txt_telefono_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void Btn_eliminar_Click(object sender, EventArgs e)
        {
            /* var idProveedor = Dgv_proveedor.CurrentRow.Cells["idproveedor"].Value;
             _proveedorManejador.eliminar(Convert.ToInt32(idProveedor));*/

            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarProveedor();
                    BuscarProveedor("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarProveedor()//necesitamos el id int para que elimine el dato
        {
            var IdProveedor = Dgv_proveedor.CurrentRow.Cells["idproveedor"].Value;//DEPENDE DEL DQATA
            _proveedorManejador.eliminar(Convert.ToInt32(IdProveedor));//EL METODO LO NECESITA INT
        }

        private void Btn_guardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                Guardarproveedor();
                LimpiarCuadros();
                BuscarProveedor("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Btn_nuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
        }

        private void Btn_cancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void txt_buscar_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txt_nombre_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txt_direccion_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void lbl_id_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Dgv_proveedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dgv_proveedor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            /*try
            {
                ModificarProveedor();
                BuscarProveedor("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }*/
        }

        private void Frm_Proveedor_Load(object sender, EventArgs e)
        {
            BuscarProveedor("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void BuscarProveedor(string filtro)
        {
            Dgv_proveedor.DataSource = _proveedorManejador.GetProveedor(filtro);
        }
        private void Guardarproveedor()
        {
            _proveedorManejador.Guardar(new Proveedor
            {
                Idproveedor = Convert.ToInt32(lbl_buscar.Text),
                Nombreproveedor = txt_nombre.Text,
                Direccion = txt_direccion.Text,
                Telefono = txt_telefono.Text
            });
        }

        private void ModificarProveedor()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lbl_buscar.Text = Dgv_proveedor.CurrentRow.Cells["idproveedor"].Value.ToString();
            txt_nombre.Text = Dgv_proveedor.CurrentRow.Cells["nombreproveedor"].Value.ToString();
            txt_direccion.Text = Dgv_proveedor.CurrentRow.Cells["direccion"].Value.ToString();
            txt_telefono.Text = Dgv_proveedor.CurrentRow.Cells["telefono"].Value.ToString();
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            Btn_nuevo.Enabled = Nuevo;
            Btn_guardar.Enabled = Guardar;
            Btn_cancelar.Enabled = Cancelar;
            Btn_eliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txt_nombre.Enabled = activar;
            txt_direccion.Enabled = activar;
            txt_idproveedor.Enabled = activar;
            txt_telefono.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txt_nombre.Text = "";
            txt_idproveedor.Text = "";
            txt_telefono.Text = "";
            txt_direccion.Text = "";
            lbl_buscar.Text = "0";
        }

        private void txt_buscar_TextChanged(object sender, EventArgs e)
        {
            BuscarProveedor(txt_buscar.Text);
        }

        private void Dgv_proveedor_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                ModificarProveedor();
                BuscarProveedor("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
