﻿namespace Abarrotesmipueblitoo
{
    partial class Frm_Proveedor
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Proveedor));
            this.label10 = new System.Windows.Forms.Label();
            this.txt_idproveedor = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_telefono = new System.Windows.Forms.MaskedTextBox();
            this.txt_buscar = new System.Windows.Forms.MaskedTextBox();
            this.txt_nombre = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_direccion = new System.Windows.Forms.MaskedTextBox();
            this.lbl_id = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Dgv_proveedor = new System.Windows.Forms.DataGridView();
            this.Btn_eliminar = new System.Windows.Forms.PictureBox();
            this.Btn_guardar = new System.Windows.Forms.PictureBox();
            this.Btn_nuevo = new System.Windows.Forms.PictureBox();
            this.Btn_cancelar = new System.Windows.Forms.PictureBox();
            this.lbl_buscar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_proveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_eliminar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_guardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_nuevo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_cancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_buscar)).BeginInit();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label10.Location = new System.Drawing.Point(47, 129);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 13);
            this.label10.TabIndex = 247;
            this.label10.Text = "Id";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // txt_idproveedor
            // 
            this.txt_idproveedor.BackColor = System.Drawing.Color.Lavender;
            this.txt_idproveedor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_idproveedor.Enabled = false;
            this.txt_idproveedor.Location = new System.Drawing.Point(30, 145);
            this.txt_idproveedor.Name = "txt_idproveedor";
            this.txt_idproveedor.Size = new System.Drawing.Size(259, 13);
            this.txt_idproveedor.TabIndex = 246;
            this.txt_idproveedor.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txt_idproveedor_MaskInputRejected);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label4.Location = new System.Drawing.Point(46, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 245;
            this.label4.Text = "Telefono";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txt_telefono
            // 
            this.txt_telefono.BackColor = System.Drawing.Color.Lavender;
            this.txt_telefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_telefono.Enabled = false;
            this.txt_telefono.Location = new System.Drawing.Point(30, 244);
            this.txt_telefono.Name = "txt_telefono";
            this.txt_telefono.Size = new System.Drawing.Size(259, 13);
            this.txt_telefono.TabIndex = 244;
            this.txt_telefono.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txt_telefono_MaskInputRejected);
            // 
            // txt_buscar
            // 
            this.txt_buscar.BackColor = System.Drawing.Color.Lavender;
            this.txt_buscar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_buscar.Enabled = false;
            this.txt_buscar.Location = new System.Drawing.Point(415, 99);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(301, 13);
            this.txt_buscar.TabIndex = 238;
            this.txt_buscar.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txt_buscar_MaskInputRejected);
            this.txt_buscar.TextChanged += new System.EventHandler(this.txt_buscar_TextChanged);
            // 
            // txt_nombre
            // 
            this.txt_nombre.BackColor = System.Drawing.Color.Lavender;
            this.txt_nombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_nombre.Enabled = false;
            this.txt_nombre.Location = new System.Drawing.Point(30, 177);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(259, 13);
            this.txt_nombre.TabIndex = 237;
            this.txt_nombre.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txt_nombre_MaskInputRejected);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label9.Location = new System.Drawing.Point(46, 197);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 236;
            this.label9.Text = "Direccion";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txt_direccion
            // 
            this.txt_direccion.BackColor = System.Drawing.Color.Lavender;
            this.txt_direccion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_direccion.Enabled = false;
            this.txt_direccion.Location = new System.Drawing.Point(30, 213);
            this.txt_direccion.Name = "txt_direccion";
            this.txt_direccion.Size = new System.Drawing.Size(259, 13);
            this.txt_direccion.TabIndex = 235;
            this.txt_direccion.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txt_direccion_MaskInputRejected);
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_id.Location = new System.Drawing.Point(752, 67);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(19, 20);
            this.lbl_id.TabIndex = 234;
            this.lbl_id.Text = "0";
            this.lbl_id.Visible = false;
            this.lbl_id.Click += new System.EventHandler(this.lbl_id_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label8.Location = new System.Drawing.Point(40, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(188, 31);
            this.label8.TabIndex = 233;
            this.label8.Text = "PROVEEDOR";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label1.Location = new System.Drawing.Point(46, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 232;
            this.label1.Text = "Nombre";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Dgv_proveedor
            // 
            this.Dgv_proveedor.BackgroundColor = System.Drawing.Color.Lavender;
            this.Dgv_proveedor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Dgv_proveedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv_proveedor.GridColor = System.Drawing.Color.SteelBlue;
            this.Dgv_proveedor.Location = new System.Drawing.Point(316, 177);
            this.Dgv_proveedor.Name = "Dgv_proveedor";
            this.Dgv_proveedor.Size = new System.Drawing.Size(401, 207);
            this.Dgv_proveedor.TabIndex = 231;
            this.Dgv_proveedor.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgv_proveedor_CellDoubleClick);
            this.Dgv_proveedor.DoubleClick += new System.EventHandler(this.Dgv_proveedor_DoubleClick);
            // 
            // Btn_eliminar
            // 
            this.Btn_eliminar.ErrorImage = null;
            this.Btn_eliminar.Image = ((System.Drawing.Image)(resources.GetObject("Btn_eliminar.Image")));
            this.Btn_eliminar.Location = new System.Drawing.Point(688, 145);
            this.Btn_eliminar.Name = "Btn_eliminar";
            this.Btn_eliminar.Size = new System.Drawing.Size(25, 26);
            this.Btn_eliminar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btn_eliminar.TabIndex = 243;
            this.Btn_eliminar.TabStop = false;
            this.Btn_eliminar.Click += new System.EventHandler(this.Btn_eliminar_Click);
            // 
            // Btn_guardar
            // 
            this.Btn_guardar.Image = global::Abarrotesmipueblitoo.Properties.Resources.icons8_save_80;
            this.Btn_guardar.Location = new System.Drawing.Point(626, 145);
            this.Btn_guardar.Name = "Btn_guardar";
            this.Btn_guardar.Size = new System.Drawing.Size(25, 26);
            this.Btn_guardar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btn_guardar.TabIndex = 242;
            this.Btn_guardar.TabStop = false;
            this.Btn_guardar.Click += new System.EventHandler(this.Btn_guardar_Click);
            // 
            // Btn_nuevo
            // 
            this.Btn_nuevo.Image = global::Abarrotesmipueblitoo.Properties.Resources.icons8_new_copy_80;
            this.Btn_nuevo.Location = new System.Drawing.Point(595, 145);
            this.Btn_nuevo.Name = "Btn_nuevo";
            this.Btn_nuevo.Size = new System.Drawing.Size(25, 26);
            this.Btn_nuevo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btn_nuevo.TabIndex = 241;
            this.Btn_nuevo.TabStop = false;
            this.Btn_nuevo.Click += new System.EventHandler(this.Btn_nuevo_Click);
            // 
            // Btn_cancelar
            // 
            this.Btn_cancelar.Image = global::Abarrotesmipueblitoo.Properties.Resources.icons8_cancel_480;
            this.Btn_cancelar.Location = new System.Drawing.Point(657, 145);
            this.Btn_cancelar.Name = "Btn_cancelar";
            this.Btn_cancelar.Size = new System.Drawing.Size(25, 26);
            this.Btn_cancelar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btn_cancelar.TabIndex = 240;
            this.Btn_cancelar.TabStop = false;
            this.Btn_cancelar.Click += new System.EventHandler(this.Btn_cancelar_Click);
            // 
            // lbl_buscar
            // 
            this.lbl_buscar.Location = new System.Drawing.Point(723, 93);
            this.lbl_buscar.Name = "lbl_buscar";
            this.lbl_buscar.Size = new System.Drawing.Size(24, 26);
            this.lbl_buscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.lbl_buscar.TabIndex = 239;
            this.lbl_buscar.TabStop = false;
            this.lbl_buscar.Click += new System.EventHandler(this.lbl_buscar_Click);
            // 
            // Frm_Proveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txt_idproveedor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_telefono);
            this.Controls.Add(this.Btn_eliminar);
            this.Controls.Add(this.Btn_guardar);
            this.Controls.Add(this.Btn_nuevo);
            this.Controls.Add(this.Btn_cancelar);
            this.Controls.Add(this.lbl_buscar);
            this.Controls.Add(this.txt_buscar);
            this.Controls.Add(this.txt_nombre);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt_direccion);
            this.Controls.Add(this.lbl_id);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dgv_proveedor);
            this.Name = "Frm_Proveedor";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Frm_Proveedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_proveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_eliminar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_guardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_nuevo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btn_cancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_buscar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txt_idproveedor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txt_telefono;
        private System.Windows.Forms.PictureBox Btn_eliminar;
        private System.Windows.Forms.PictureBox Btn_guardar;
        private System.Windows.Forms.PictureBox Btn_nuevo;
        private System.Windows.Forms.PictureBox Btn_cancelar;
        private System.Windows.Forms.PictureBox lbl_buscar;
        private System.Windows.Forms.MaskedTextBox txt_buscar;
        private System.Windows.Forms.MaskedTextBox txt_nombre;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox txt_direccion;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView Dgv_proveedor;
    }
}

