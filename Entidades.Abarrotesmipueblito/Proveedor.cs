﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Abarrotesmipueblito
{
    public class Proveedor
    {
        private int idproveedor;
        private string nombreproveedor;
        private string direccion;
        private string telefono;

        public int Idproveedor { get => idproveedor; set => idproveedor = value; }
        public string Nombreproveedor { get => nombreproveedor; set => nombreproveedor = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Telefono { get => telefono; set => telefono = value; }
    }
}
