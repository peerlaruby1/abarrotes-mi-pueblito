﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Abarrotesmipueblito;
using System.Data;

namespace AccesoDatos.Abarrotesmipueblito
{
    public class ProveedorAccesoDatos
    {

        ConexionAccesoDatos conexion;
        public ProveedorAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "abarrotesmipueblito", 3306);
        }
        public void Guardar(Proveedor proveedor)
        {
            if (proveedor.Idproveedor == 0)
            {
                //Insertar
                string consulta = string.Format("Insert into proveedor values(null,'{0}','{1}','{2}') ", proveedor.Nombreproveedor, proveedor.Direccion, proveedor.Telefono);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update proveedor set nombreproveedor = '{0}', direccion = '{1}',telefono = '{2}' where idproveedor = {3}", proveedor.Nombreproveedor, proveedor.Direccion,proveedor.Telefono, proveedor.Idproveedor);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void eliminar(int idProveedor)
        {
            //Eliminar
            string consulta = string.Format("Delete from proveedor where idproveedor= '{0}'", idProveedor);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Proveedor> GetProveedor(string filtro)
        {
            var Listproveedor = new List<Proveedor>();// SE ALAMCENARA EN ESTE USUARIO 
            var ds = new DataSet();
            string consulta = "select * from proveedor where nombreproveedor like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "proveedor");// el data set se recoorre para la lista
            var dt = new DataTable();
            dt = ds.Tables[0];//queremos la tabla 0

            foreach (DataRow row in dt.Rows)
            {
                var proveedor = new Proveedor
                {
                    Idproveedor = Convert.ToInt32(row["idproveedor"]),
                    Nombreproveedor = row["nombreproveedor"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),

                };
                Listproveedor.Add(proveedor);
            }
            return Listproveedor;
        }

    }
}
