create database abarrotesmipueblito;
use abarrotesmipueblito;

create table proveedor(
idproveedor int primary key auto_increment,
nombreproveedor varchar(100),
direccion varchar(100),
telefono varchar(10)
);

create table compras (
idcompras int primary key auto_increment,
cantidad double,
fechacompra date,
fkproveedor int, 
fkproducto int,
foreign key (fkproveedor) references proveedor(idproveedor),
foreign key (fkproducto) references producto(idproducto)
);

insert into categoria values(null,'Pan');
insert into categoria values (null, 'Bebidas');

insert into producto values(null,'donitas bimbo',10.50,1);
insert into producto values(null,'pan bimbo',35,1);

insert into producto values(null,'cocacola',12,2);
insert into producto values(null,'agua bonafont',10,2);

insert into proveedor values(null, 'bimbo','juarez #33','7422233');
insert into proveedor values(null, 'cocacola','allende #8','7424444');
insert into proveedor values(null, 'bonafont','hidalgo #12','7488884');

insert into compras values(null,10,'2020-02-12',1,1);
insert into compras values(null,10,'2020-02-12',2,3);

create view v_proveedor as 
select nombreproveedor, producto.nombre as nombreproducto, precio, fechacompra,cantidad, categoria.nombre as categoria from proveedor, compras, producto,categoria where compras.fkproducto=producto.idproducto and compras.fkproveedor=proveedor.idproveedor;
select * from v_proveedor;
/*
drop view v_proveedor;
select nombreproveedor,nombre, precio, fechacompra,cantidad from proveedor, compras, producto where compras.fkproducto=producto.idproducto and compras.fkproveedor=proveedor.idproveedor;
select nombreproveedor,nombre, precio, fechacompra,cantidad from proveedor, compras, producto where fkproducto=idproducto and fkproveedor=idproveedor;
select * from compras;
*/

create procedure computrabadahaha(in _canditad double , in _fechacompr date, in _fkproveedor int, in _fkproducto int)
begin 
if _cantidad <=1 then
insert into compras values(null,_cantidad,_fechacompra,_fkproveedor,fkproducto);
else
select "El campo es menor a 0. ";
end if;
end;

select nombreproducto, 
categoria 
FROM v_proveedor
order by categoria;


/*select nombre from producto;*/
/*select nombre, fkcategoria FROM producto order by fkcategoria;
select nombre FROM producto, nombre from categoria where order by fkcategoria;*/
